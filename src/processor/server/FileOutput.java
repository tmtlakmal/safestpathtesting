package processor.server;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import common.Settings;
import common.SysUtil;
import processor.communication.message.SerializableRouteDump;
import processor.communication.message.SerializableRouteDumpPoint;
import processor.communication.message.SerializableTrajectory;
import processor.communication.message.SerializableVehicleTravelTime;

public class FileOutput {
	FileOutputStream fosLog;
	FileOutputStream fosVehicleTravelTime;
	FileOutputStream fosTrajectory;
	FileOutputStream fosRoute;

	/**
	 * Close output file
	 */
	void close() {
		try {
			if (fosLog != null) {
				fosLog.close();
			}
			if (fosVehicleTravelTime != null) {
				fosVehicleTravelTime.close();
			}
			if (fosTrajectory != null) {
				fosTrajectory.close();
			}
			if (fosRoute != null) {
				outputStringToFile(fosRoute, "</data>");
				fosRoute.close();
			}
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void init() {
		if (Settings.isOutputForegroundTravelTime) {
			initVehicleTravelTimeOutputFile();
		}
		if (Settings.isOutputForegroundTrajectory) {
			initTrajectoryOutputFile();
		}
		if (Settings.isOutputInitialRouteOfRandomVehicles) {
			initRouteOutputFile();
		}
		if (Settings.isOutputSimulationLog) {
			initSimLogOutputFile();
		}
	}

	File getNewFile(String prefix) {
		String fileName = prefix + SysUtil.getTimeStampString() + ".txt";
		File file = new File(fileName);
		int counter = 0;
		while (file.exists()) {
			counter++;
			fileName = prefix + SysUtil.getTimeStampString() + "_" + counter + ".txt";
			file = new File(fileName);
		}
		return file;
	}

	void initRouteOutputFile() {
		try {
			final File file = getNewFile(Settings.prefixOutputRoutePlan);
			fosRoute = new FileOutputStream(file, true);
			outputStringToFile(fosRoute,
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>" + System.getProperty("line.separator"));
			outputStringToFile(fosRoute, "<data>" + System.getProperty("line.separator"));
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void initSimLogOutputFile() {

		try {
			final File file = getNewFile(Settings.prefixOutputSimLog);
			// Print column titles
			fosLog = new FileOutputStream(file, true);
			outputStringToFile(fosLog, "Time Stamp, Real Time(s), Simulation Time(s), # of Worker-Worker Connections"
					+ System.getProperty("line.separator"));
		} catch (final Exception e) {
			e.printStackTrace();
		}
	}

	void initTrajectoryOutputFile() {
		try {
			final File file = getNewFile(Settings.prefixOutputTrajectory);
			// Print column titles
			fosTrajectory = new FileOutputStream(file, true);
			outputStringToFile(fosTrajectory,
					"Trajectory ID,Vehicle ID,Time Stamp,Latitude,Longitude" + System.getProperty("line.separator"));
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	void initVehicleTravelTimeOutputFile() {
		try {
			final File file = getNewFile(Settings.prefixOutputForegroundTravelTime);
			// Print column titles
			fosVehicleTravelTime = new FileOutputStream(file, true);
			outputStringToFile(fosVehicleTravelTime,
					"Vehicle Id,Travel Time (s),Average Speed (kmh)" + System.getProperty("line.separator"));
		} catch (final IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Output trajectory of individual vehicles
	 */
	int outputTrajectories(int trajectoryId, final ArrayList<SerializableTrajectory> trajectoryList) {
		if (Settings.isOutputForegroundTrajectory) {
			for (final SerializableTrajectory serializableTrajectory : trajectoryList) {
				trajectoryId++;

				for (int i = 0; i < serializableTrajectory.trajectoryPoints.size(); i++) {
					if (i == 0) {
						outputStringToFile(fosTrajectory, String.valueOf(trajectoryId));
					}
					outputStringToFile(fosTrajectory, ",");
					outputStringToFile(fosTrajectory, serializableTrajectory.vehicleId);
					outputStringToFile(fosTrajectory, ",");
					outputStringToFile(fosTrajectory,
							String.valueOf(serializableTrajectory.trajectoryPoints.get(i).realTime));
					outputStringToFile(fosTrajectory, ",");
					outputStringToFile(fosTrajectory,
							String.valueOf(serializableTrajectory.trajectoryPoints.get(i).lat));
					outputStringToFile(fosTrajectory, ",");
					outputStringToFile(fosTrajectory,
							String.valueOf(serializableTrajectory.trajectoryPoints.get(i).lon));
					outputStringToFile(fosTrajectory, System.getProperty("line.separator"));

				}
			}
		}
		return trajectoryId;
	}

	void outputRoutes(final ArrayList<SerializableRouteDump> routes) {
		if (Settings.isOutputInitialRouteOfRandomVehicles) {
			try {
				final StringBuilder sb = new StringBuilder();
				for (final SerializableRouteDump route : routes) {
					sb.append("<vehicle ");
					sb.append("id=\"" + route.vehicleId + "\" type=\"" + route.type + "\" start_time=\""
							+ route.startTime + "\" driverProfile=\"" + route.driverProfile + "\">"
							+ System.getProperty("line.separator"));
					for (final SerializableRouteDumpPoint point : route.routeDumpPoints) {
						sb.append("<node ");
						if (point.stopDuration == 0) {
							sb.append("id=\"" + point.nodeId + "\"/>" + System.getProperty("line.separator"));
						} else {
							sb.append("id=\"" + point.nodeId + "\" stopover=\"" + point.stopDuration + "\"/>"
									+ System.getProperty("line.separator"));
						}
					}
					sb.append("</vehicle>" + System.getProperty("line.separator"));

				}
				outputStringToFile(fosRoute, sb.toString());
			} catch (final Exception e) {
				e.printStackTrace();
			}
		}
	}

	void outputSimLog(final int stepCurrent, final double simulationTimeCounter, final int totalNumFellowsOfWorker) {
		final Date date = new Date();

		if (Settings.isOutputSimulationLog && (fosLog != null)) {
			outputStringToFile(fosLog, date.toString());
			outputStringToFile(fosLog, ",");
			outputStringToFile(fosLog, String.valueOf(stepCurrent / Settings.numStepsPerSecond));
			outputStringToFile(fosLog, ",");
			outputStringToFile(fosLog, String.valueOf(simulationTimeCounter));
			outputStringToFile(fosLog, ",");
			outputStringToFile(fosLog, String.valueOf(totalNumFellowsOfWorker));
			outputStringToFile(fosLog, System.getProperty("line.separator"));
		}
	}

	void outputStringToFile(final FileOutputStream fos, final String str) {

		final byte[] dataInBytes = str.getBytes();

		try {
			fos.write(dataInBytes);
			fos.flush();
		} catch (final IOException e) {
		}
	}

	void outputVehicleTravelTimes(final ArrayList<SerializableVehicleTravelTime> travelTime) {
		for (final SerializableVehicleTravelTime item : travelTime) {
			if (Settings.isOutputForegroundTravelTime) {
				outputStringToFile(fosVehicleTravelTime, item.vehicleId);
				outputStringToFile(fosVehicleTravelTime, ",");
				outputStringToFile(fosVehicleTravelTime, String.valueOf(item.time));
				outputStringToFile(fosVehicleTravelTime, ",");
				outputStringToFile(fosVehicleTravelTime, String.valueOf(item.avgSpd));
				outputStringToFile(fosVehicleTravelTime, System.getProperty("line.separator"));
			}
		}
	}
}
