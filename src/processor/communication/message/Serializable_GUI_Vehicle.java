package processor.communication.message;

public class Serializable_GUI_Vehicle {
	public String type;
	public double speed;
	public double lonHead;
	public double latHead;
	public double lonTail;
	public double latTail;
	public int numLinksToGo;
	public String idWorker;
	public String driverProfile;
	public int edgeIndex;
	public double originalEdgeMaxSpeed;
	public boolean isAffectedByPriorityVehicle;

	public Serializable_GUI_Vehicle() {

	}
}
